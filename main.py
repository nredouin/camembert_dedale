# attention dans les dépendances avec python 2.7 one ne pouvait que telecharger plotly 4.14.3 et pas 5.8.0

import torch
from torch.utils.data import DataLoader
import torch.nn.functional as F
import pytorch_lightning as pl
from transformers import AutoModelForSequenceClassification, CamembertForMaskedLM, AutoConfig, Trainer, \
    TrainingArguments, AutoTokenizer, DefaultDataCollator
from datasets import load_dataset
import torch.nn.functional as F
from datasets import load_dataset
from tokenizer import tokenize_function
import numpy as np
# from evaluated import compute_metrics
import utils


camembert = CamembertForMaskedLM.from_pretrained("dedale")
# tokenizer = AutoTokenizer.from_pretrained('tokenizer')
from sklearn.model_selection import ShuffleSplit

def trainCamemBERT():
    raw_datasets = load_dataset('csv', data_files="C:\\Users\\noere\\code\\camemBERT_dedale\\Classeur1.csv",
                                sep=";", header=None, encoding="ISO-8859-1")
    print("loaded")
    tokenized_datasets = raw_datasets.map(tokenize_function, batched=True)
    tokenized_datasets.remove_columns(['0'])
    # tokenized_datasets = tokenized_datasets.with_format('torch')
    print(DefaultDataCollator(tokenized_datasets))
    print(tokenized_datasets.column_names)
    print(tokenized_datasets['train'][
              'input_ids'])  # avec ça on voit quelle mot a été attrobué à quel mot référencé dans le RNA
    rs = ShuffleSplit(n_splits=1, test_size=.25, random_state=0)

    for train_index, test_index in rs.split(range(0, 276)):
        pass

    small_train_dataset = tokenized_datasets["train"].select(train_index)
    small_eval_dataset = tokenized_datasets["train"].select(test_index)
    print(small_train_dataset.features)
    training_args = TrainingArguments(output_dir="test_trainer", evaluation_strategy="epoch")
    trainer = Trainer(
        model=camembert,
        train_dataset=small_train_dataset,
        eval_dataset=small_eval_dataset,
        args=training_args
        # compute_metrics=compute_metrics
    )
    # trainer.train()














# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    trainCamemBERT()
    # print(camembert.roberta.embeddings)
    # batch_sentences = [
    #     "Vous savez où est la piscine la plus proche?",
    #     "La Seine est un fleuve.",
    #     "Je cherche urgemment un endroit où retirer de l'argent.",
    # ]
    # utils.simi2phrases("je n'arrive pas à me laver tout seul", "j'ai besoin d'aide pour faire ma toilette")
