def trainCamemBERT():
    raw_datasets = load_dataset('csv', data_files="C:\\Users\\noere\\code\\camemBERT_dedale\\textPDFetBDDnonTraite.csv",
                                sep=";",
                                header=None)
    print(raw_datasets)
    tokenized_datasets = raw_datasets.map(tokenize_function, batched=True)
    tokenized_datasets.remove_columns(['0'])
    tokenized_datasets = tokenized_datasets.with_format('torch')
    print(tokenized_datasets.column_names)
    print(tokenized_datasets['train'][
              'input_ids'])  # avec ça on voit quelle mot a été attrobué à quel mot référencé dans le RNA
    rs = ShuffleSplit(n_splits=1, test_size=.25, random_state=0)

    for train_index, test_index in rs.split(range(0, 276)):
        pass

    small_train_dataset = tokenized_datasets["train"].select(train_index)
    small_eval_dataset = tokenized_datasets["train"].select(test_index)
    print(small_train_dataset.features)
    training_args = TrainingArguments(output_dir="test_trainer", evaluation_strategy="epoch")
    trainer = Trainer(
        model=camembert,
        train_dataset=small_train_dataset,
        eval_dataset=small_eval_dataset,
        args=training_args
        # compute_metrics=compute_metrics
    )
    trainer.train()