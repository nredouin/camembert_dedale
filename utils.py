import torch
from transformers import AutoTokenizer
import torch.nn.functional as F
import pprint
from main import camembert


def take_first_embedding(embeddings, attention_mask=None):
    return embeddings[:, 0]


def average_embeddings(embeddings, attention_mask):
    return (attention_mask[..., None] * embeddings).mean(1)


tokenizer = AutoTokenizer.from_pretrained("tokenizer")


def tokenize(batch_sentences):
    """
    - The function takes in a batch of sentences and a tokenizer.
    - It then tokenizes the sentences using the tokenizer.
    - It returns the tokenized sentences.

    Let's see how it works.

    We'll use the tokenizer we created earlier.

    We'll also create a batch of sentences.

    We'll then tokenize the batch of sentences using the tokenizer.

    We'll then print the tokenized sentences.


    :param batch_sentences: The sentences to be tokenized
    :param tokenizer: The tokenizer to use
    :return: The tokenizer_output is a dictionary with the keys:
        - input_ids
        - attention_mask
        - token_type_ids
        - labels
    """
    tokenizer_output = tokenizer(
        batch_sentences,
        padding="max_length",
        truncation=True,
        return_tensors="pt"
    )
    return tokenizer_output


def simi2phrases(p1, p2):
    batch_sentences = []
    batch_sentences.append(p1)
    batch_sentences.append(p2)
    tokenized = tokenize(batch_sentences)
    with torch.no_grad():
        model_output = camembert(**tokenized, output_hidden_states=True)
    token_embeddings = model_output.hidden_states[-1]

    print(token_embeddings)
    # pprint([tokenizer.convert_ids_to_tokens(input_ids) for input_ids in tokenized['input_ids']], width=150)

    moy_phrase = average_embeddings(token_embeddings, tokenized.attention_mask)
    print(moy_phrase)
    avg_similarity_score = F.cosine_similarity(moy_phrase[0], moy_phrase[1], dim=-1)
    print("similarité entre les deux phhrases : ", avg_similarity_score)

    # for sent_id_1, sent_id_2 in [[0, 1], [2, 1], [2, 0]]:
    #     first_tok_similarity_score = F.cosine_similarity(first_tok_sentence_representations[sent_id_1],
    #                                                      first_tok_sentence_representations[sent_id_2], dim=-1)
    #     avg_similarity_score = F.cosine_similarity(avg_sentence_representations[sent_id_1],
    #                                                avg_sentence_representations[sent_id_2], dim=-1)
    #
    #     print(f"{batch_sentences[sent_id_1]}    vs.    {batch_sentences[sent_id_2]}")
    #     print(f"Score (first_tok) : {first_tok_similarity_score}")
    #     print(f"Score (average) : {avg_similarity_score}\n")
