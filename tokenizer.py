from transformers import AutoTokenizer

tokenizer = AutoTokenizer.from_pretrained('tokenizer')


def tokenize_function(example):
    return tokenizer(
        example["0"], padding="max_length"
    )

